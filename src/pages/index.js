import LoginPage from "./LoginPage/LoginPage";
import RegisterPage from "./RegisterPage/RegisterPage";
import HomePage from "./HomePage/HomePage";

export {
    LoginPage,
    RegisterPage,
    HomePage,
}
import './App.css';
import {LoginPage, RegisterPage, HomePage} from './pages/index'

import {Routes, Route, Link} from 'react-router-dom';

function App() {
  return (
    <div className="App">

      <Link to='/'>Home </Link>
      <Link to='/register'>Register </Link>
      <Link to='/login'>Login </Link>

      <Routes>
        <Route path='/' element= {<HomePage/>} />
        <Route path='/register' element= {<RegisterPage/>} />
        <Route path='/login' element= {<LoginPage/>} />

      </Routes>

    </div>
  );
}

export default App;
